//
//  SuperheroTableViewController.swift
//  superheroesandlaureates
//
//  Created by Student on 4/13/19.
//  Copyright © 2019 Injamuri, Satyavrath. All rights reserved.
//

import UIKit

class SuperheroTableViewController: UITableViewController {

    let shero = "https://www.dropbox.com/s/wpz5yu54yko6e9j/squad.json?dl=1"
    var superheros:[Members] = []
   
    func displaySuperherodata(data:Data?, urlResponse:URLResponse?, error:Error?)->Void {
        
        do {
            let decoder:JSONDecoder = JSONDecoder()
            superheros = try decoder.decode(Superheros.self, from: data!).members
            DispatchQueue.main.async {
                self.tableView.reloadData()
                NotificationCenter.default.post(name: NSNotification.Name("Superheros home coming"), object: self.superheros)
            }
        } catch {
            print("something went wrong")
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let urlSession = URLSession.shared
        let url = URL(string: shero)
        urlSession.dataTask(with: url!, completionHandler: displaySuperherodata).resume()
        
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    
    // MARK: - Table view data source
    
    
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return superheros.count
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "superhero", for: indexPath)
        
        // Configure the cell...
        cell.textLabel?.text = "\(superheros[indexPath.row].name) (aka: \(superheros[indexPath.row].secretIdentity))"
        var inputpower = superheros[indexPath.row].powers
        var power: String = ""
        for i in stride(from: 0, to: inputpower.count, by: 1){
            if i < inputpower.count - 1{
                power = power + inputpower[i] + ", "
            }else{
                power = power + inputpower[i]
            }
        }
        cell.detailTextLabel?.text = "\(power)"
        
        return cell
        
    }


    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
